# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "5.31.0"
  hashes = [
    "h1:ltxyuBWIy9cq0kIKDJH1jeWJy/y7XJLjS4QrsQK4plA=",
    "zh:0cdb9c2083bf0902442384f7309367791e4640581652dda456f2d6d7abf0de8d",
    "zh:2fe4884cb9642f48a5889f8dff8f5f511418a18537a9dfa77ada3bcdad391e4e",
    "zh:36d8bdd72fe61d816d0049c179f495bc6f1e54d8d7b07c45b62e5e1696882a89",
    "zh:539dd156e3ec608818eb21191697b230117437a58587cbd02ce533202a4dd520",
    "zh:6a53f4b57ac4eb3479fc0d8b6e301ca3a27efae4c55d9f8bd24071b12a03361c",
    "zh:6faeb8ff6792ca7af1c025255755ad764667a300291cc10cea0c615479488c87",
    "zh:7d9423149b323f6d0df5b90c4d9029e5455c670aea2a7eb6fef4684ba7eb2e0b",
    "zh:8235badd8a5d0993421cacf5ead48fac73d3b5a25c8a68599706a404b1f70730",
    "zh:860b4f60842b2879c5128b7e386c8b49adeda9287fed12c5cd74861bb659bbcd",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:b021fceaf9382c8fe3c6eb608c24d01dce3d11ba7e65bb443d51ca9b90e9b237",
    "zh:b38b0bfc1c69e714e80cf1c9ea06e687ee86aa9f45694be28eb07adcebbe0489",
    "zh:c972d155f6c01af9690a72adfb99cfc24ef5ef311ca92ce46b9b13c5c153f572",
    "zh:e0dd29920ec84fdb6026acff44dcc1fb1a24a0caa093fa04cdbc713d384c651d",
    "zh:e3127ebd2cb0374cd1808f911e6bffe2f4ac4d84317061381242353f3a7bc27d",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "2.11.0"
  constraints = "2.11.0"
  hashes = [
    "h1:5d/H845zFmYP3IG1iF3TqiPIQ+uePnVHJNCCxt/2EuQ=",
    "zh:0fe2901a564ea23b4b5c9b514f0212c8495d99f041dcf2b9cac900747d5c4445",
    "zh:11afd91892d5145b3479f726665881622849ba29299a09c37d8bc02f4b3f99b8",
    "zh:34b3b5262c26ff3c08fdecf22a0db285d80f5461f70d622042bbad3fb93d8f9e",
    "zh:4d8d73ed90f4b79e1c43eb3a864954b5c715086a0daf5110bb82480e55f0da0a",
    "zh:4f8adf4423f1a292b4d0a265b8b71c80dd3a20db216e5ed7240e2a06765b36fc",
    "zh:5035f9660f698467e8655093fe2c55ce868bc49990745ade9d0bc553fba367ae",
    "zh:82aa22c734b3ab03a2f27f37d5444eafb6f70d6a6de0057a81b1131dfb0177e7",
    "zh:921ccdcb16ce0235c13f734020bdaee1f9ac2f9c1376f4400d78ba2627f63fc9",
    "zh:94ae34ad3aa4c60f16e68046730cd4bab1cf811ea1195ce2ed4b03e6f2111571",
    "zh:96c09e7d5b1a72b5b1cdffbafe5a4a964af089bc3e7f650e0a20c7e2e44ef8c7",
    "zh:d4c1f2acf5030704fb5441e1bd70bbf8d66821f6e8ef44a073fcd619fff7ecd2",
    "zh:dfbf5e7533d3f759b5aa1f7a3d81e158e09a54b3823c232a57161e50b978910f",
    "zh:f6583fef9d14c21058a5bc8b39c16808c2e85c889dd9380c2d636f2f3c132617",
    "zh:f6ee6c3cf89ad2257d6f11035d833a61536e81c4a41d203b5cdb8916b033fe73",
  ]
}
