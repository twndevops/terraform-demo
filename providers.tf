# global definition of installed Providers/ versions
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.31.0"
    }
#    linode = {
#      source = "linode/linode"
#      version = "2.11.0"
#    }
  }
}