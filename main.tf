# TF will programmatically connect to twnadmin user's AWS acct
provider "aws" {
  # since region and keys are in .aws/config and .aws/credentials, no need to hard-code below
  # region = "us-east-1"
  # configured AWS user must have permissions so TF can achieve desired state/ query resources
  # access_key = "AKIAUKVPVGSO2SAJDKMP"
  # secret_key = "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"
}

# Configure the Linode Provider
# provider "linode" {
  # token = "..."
# }

variable "vpc_cidr_block" {
  description = "vpc ip range"
}

variable "environment" {
  description = "AWS vpc name"
}

variable "cidr_blocks" {
  description = "cidrs and name tags for vpc and subnets"
  # type = list(string)
  type = list(object({
    cidr_block = string
    name = string
  }))
}

resource "aws_vpc" "dev-vpc" {
  # cidr_block = "10.0.0.0/16"
  # cidr_block = var.vpc_cidr_block
  # cidr_block = var.cidr_blocks[0]
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    # Name: "terraform-demo-vpc"
    # Name: var.environment
    Name: var.cidr_blocks[0].name
    # vpc_env: "dev"
  }
}

variable "subnet_cidr_block" {
  description = "subnet ip range"
  # passed 10.0.20.0/24 via user prompt
  # passed 10.0.30.0/24 via -var option
  default = "10.0.10.0/24"
  type = string
}

# can leave variable blocks empty
# custom TF env var exported in Terminal session
variable "az" {}

# creates private subnet (no IPv4 or IPv6)
resource "aws_subnet" "dev-subnet-1" {
  # VPC in which subnet should be created ([resource type].[resource name].[id])
  vpc_id = aws_vpc.dev-vpc.id
  # cidr_block = "10.0.10.0/24" # passed as initial value
  # cidr_block = var.subnet_cidr_block
  # cidr_block = var.cidr_blocks[1]
  cidr_block = var.cidr_blocks[1].cidr_block
  # availability_zone = "us-east-1c"
  availability_zone = var.az
  tags = {
    # Name: "terraform-demo-subnet-1-demo-vpc"
    Name: var.cidr_blocks[1].name
  }
}

# # queried default VPC
# data "aws_vpc" "existing-vpc" {
#   default = true
# }

# # created new subnet in default VPC, but it was NOT a default subnet
# # default subnets (6) all have public IPv4s, but this one won't (private)
# resource "aws_subnet" "dev-subnet-2" {
#   # data prefix distinguishes from resource component
#   vpc_id = data.aws_vpc.existing-vpc.id
#   # IP range in VPC CIDR that doesn't conflict w/ existing Subnets' CIDRs
#   cidr_block = "172.31.96.0/20"
#   availability_zone = "us-east-1e"
#   tags = {
#     Name: "terraform-demo-subnet-2-default-vpc"
#   }
# }

# like a function return value
# output "dev-vpc-id" {
#   value = aws_vpc.dev-vpc.id
# }

# output "dev-subnet-1-id" {
#   value = aws_subnet.dev-subnet-1.id
# }